// gtest
#include <benchmark/benchmark.h>   // googletest header file

// cc library
#include <engine.h>
#include <alphabeta_ai.h>
#include <minimax_ai.h>

#include <chrono>
using namespace std::chrono_literals;

namespace detail
{

  // Declarations
  auto generateStartPositionBitBoard();
  auto simulationMethod(othello::detail::PlayerStruct& AI_player_one, othello::detail::PlayerStruct& AI_player_two);
  auto generatePositionWith14LegalMoves();

  // Definitions
  auto generateStartPositionBitBoard()
  {

    const othello::BitPieces white_starter_pieces{"00000000"
                                                  "00000000"
                                                  "00000000"
                                                  "00010000"
                                                  "00001000"
                                                  "00000000"
                                                  "00000000"
                                                  "00000000"};
    const othello::BitPieces black_starter_pieces{"00000000"
                                                  "00000000"
                                                  "00000000"
                                                  "00001000"
                                                  "00010000"
                                                  "00000000"
                                                  "00000000"
                                                  "00000000"};

    return othello::BitBoard{white_starter_pieces, black_starter_pieces};
  }

  auto generatePositionWith14LegalMoves(){

      const othello::BitPieces white_starter_pieces{"00000000"
                                                    "00100000"
                                                    "00100000"
                                                    "00011110"
                                                    "00000000"
                                                    "00000000"
                                                    "00000000"
                                                    "00000000"};
      const othello::BitPieces black_starter_pieces{"00000000"
                                                    "00000000"
                                                    "01011100"
                                                    "00100000"
                                                    "01111100"
                                                    "00000000"
                                                    "00000000"
                                                    "00000000"};

      return othello::BitBoard{white_starter_pieces, black_starter_pieces};
  }
}   // namespace detail

auto simulationMethod(othello::detail::PlayerStruct& AI_player_one, othello::detail::PlayerStruct& AI_player_two){
    othello::BitBoard bitboard = detail::generateStartPositionBitBoard();

    int occPos = 4;
    int turn = 0;
    int passed = 0;
    while (occPos != 64 && passed != 2){
        int nmoves;
        if (turn == 0) {
            nmoves = othello::utility::legalMoves(bitboard, othello::PlayerId::One).size();
            if (nmoves > 0) {
                AI_player_one.obj->think(bitboard, othello::PlayerId::One, 1000s);
                bitboard = othello::utility::placeAndFlip(bitboard, AI_player_one.obj->bestMove(), othello::PlayerId::One);
                turn = 1;
            }
            else {
                passed++;
                turn = 1;
            }
        }
        else {
            nmoves = othello::utility::legalMoves(bitboard, othello::PlayerId::Two).size();
            if (nmoves > 0) {
                AI_player_two.obj->think(bitboard,othello::PlayerId::Two, 1000s);
                bitboard = othello::utility::placeAndFlip(bitboard, AI_player_two.obj->bestMove(), othello::PlayerId::Two);
                turn = 0;
            }
            else {
                passed++;
                turn = 0;
            }
        }

        occPos = othello::utility::countingScore(othello::PlayerId::One, bitboard)
                + othello::utility::countingScore(othello::PlayerId::Two, bitboard);
    }

    return bitboard;
}

static void legalMovesFromInitBoard(benchmark::State& state)
{
  const auto bitboard = detail::generateStartPositionBitBoard();

//  for (auto _ : state) {
  while( state.KeepRunning()) {
    auto legal_moves
      = othello::utility::legalMoves(bitboard, othello::PlayerId::One);
    benchmark::DoNotOptimize(legal_moves);
  }
}

static void legalMovesFromMiddleBoard(benchmark::State& state)
{
  const auto bitboard = detail::generatePositionWith14LegalMoves();

//  for (auto _ : state) {
  while( state.KeepRunning()) {
    auto legal_moves
      = othello::utility::legalMoves(bitboard, othello::PlayerId::One);
    benchmark::DoNotOptimize(legal_moves);
  }
}

static void thinkMethodAlphabeta(benchmark::State& state)
{
  const auto bitboard = detail::generateStartPositionBitBoard();
  othello::detail::PlayerStruct AI_player;
  AI_player.obj = std::make_unique<othello::alphabeta_ais::AlphabetaAI>(othello::PlayerId::One);

//  for (auto _ : state) {
  while( state.KeepRunning()) {
      othello::BitPos best_move;
      AI_player.obj->think(bitboard, othello::PlayerId::One, 200s);
      best_move = AI_player.obj->bestMove();
    benchmark::DoNotOptimize(best_move);
  }
}

static void thinkMethodAlphabeta14Moves(benchmark::State& state)
{
  const auto bitboard = detail::generatePositionWith14LegalMoves();
  othello::detail::PlayerStruct AI_player;
  AI_player.obj = std::make_unique<othello::alphabeta_ais::AlphabetaAI>(othello::PlayerId::One);

//  for (auto _ : state) {
  while( state.KeepRunning()) {
      othello::BitPos best_move;
      AI_player.obj->think(bitboard, othello::PlayerId::One, 300s);
      best_move = AI_player.obj->bestMove();
    benchmark::DoNotOptimize(best_move);
  }
}

static void thinkMethodMinimax(benchmark::State& state)
{
  const auto bitboard = detail::generateStartPositionBitBoard();
  othello::detail::PlayerStruct AI_player;
  AI_player.obj = std::make_unique<othello::minimax_ais::MinimaxAI>(othello::PlayerId::One);

//  for (auto _ : state) {
  while( state.KeepRunning()) {
      othello::BitPos best_move;
      AI_player.obj->think(bitboard, othello::PlayerId::One, 200s);
      best_move = AI_player.obj->bestMove();
    benchmark::DoNotOptimize(best_move);
  }
}

static void thinkMethodMinimax14Moves(benchmark::State& state)
{
  const auto bitboard = detail::generatePositionWith14LegalMoves();
  othello::detail::PlayerStruct AI_player;
  AI_player.obj = std::make_unique<othello::minimax_ais::MinimaxAI>(othello::PlayerId::One);

//  for (auto _ : state) {
  while( state.KeepRunning()) {
      othello::BitPos best_move;
      AI_player.obj->think(bitboard, othello::PlayerId::One, 400s);
      best_move = AI_player.obj->bestMove();
    benchmark::DoNotOptimize(best_move);
  }
}

static void thinkMethodOrangeMonkey(benchmark::State& state)
{
  const auto bitboard = detail::generateStartPositionBitBoard();
  othello::detail::PlayerStruct AI_player;
  AI_player.obj = std::make_unique<othello::monkey_ais::OrangeMonkeyAI>(othello::PlayerId::One);

//  for (auto _ : state) {
  while( state.KeepRunning()) {
      othello::BitPos best_move;
      AI_player.obj->think(bitboard, othello::PlayerId::One, 200s);
      best_move = AI_player.obj->bestMove();
    benchmark::DoNotOptimize(best_move);
  }
}

static void thinkMethodOrangeMonkey14Moves(benchmark::State& state)
{
  const auto bitboard = detail::generatePositionWith14LegalMoves();
  othello::detail::PlayerStruct AI_player;
  AI_player.obj = std::make_unique<othello::monkey_ais::OrangeMonkeyAI>(othello::PlayerId::One);

//  for (auto _ : state) {
  while( state.KeepRunning()) {
      othello::BitPos best_move;
      AI_player.obj->think(bitboard, othello::PlayerId::One, 200s);
      best_move = AI_player.obj->bestMove();
    benchmark::DoNotOptimize(best_move);
  }
}

static void simulationOrangeMonkey(benchmark::State& state)
{
  othello::detail::PlayerStruct AI_player_one, AI_player_two;
  AI_player_one.obj = std::make_unique<othello::monkey_ais::OrangeMonkeyAI>(othello::PlayerId::One);
  AI_player_two.obj = std::make_unique<othello::monkey_ais::OrangeMonkeyAI>(othello::PlayerId::Two);

//  for (auto _ : state) {
  while( state.KeepRunning()) {
      othello::BitBoard bitboard = simulationMethod(AI_player_one, AI_player_two);
    benchmark::DoNotOptimize(bitboard);
  }
}

static void simulationMinimax(benchmark::State& state)
{
  othello::detail::PlayerStruct AI_player_one, AI_player_two;
  AI_player_one.obj = std::make_unique<othello::minimax_ais::MinimaxAI>(othello::PlayerId::One);
  AI_player_two.obj = std::make_unique<othello::minimax_ais::MinimaxAI>(othello::PlayerId::Two);

//  for (auto _ : state) {
  while( state.KeepRunning()) {
      othello::BitBoard bitboard = simulationMethod(AI_player_one, AI_player_two);
    benchmark::DoNotOptimize(bitboard);
  }
}

static void simulationAlphabeta(benchmark::State& state)
{
  othello::detail::PlayerStruct AI_player_one, AI_player_two;
  AI_player_one.obj = std::make_unique<othello::alphabeta_ais::AlphabetaAI>(othello::PlayerId::One);
  AI_player_two.obj = std::make_unique<othello::alphabeta_ais::AlphabetaAI>(othello::PlayerId::Two);

//  for (auto _ : state) {
  while( state.KeepRunning()) {
      othello::BitBoard bitboard = simulationMethod(AI_player_one, AI_player_two);
    benchmark::DoNotOptimize(bitboard);
  }
}


BENCHMARK(legalMovesFromInitBoard);
BENCHMARK(legalMovesFromMiddleBoard);
BENCHMARK(thinkMethodAlphabeta);
BENCHMARK(thinkMethodAlphabeta14Moves);
BENCHMARK(simulationAlphabeta);
BENCHMARK(thinkMethodMinimax);
BENCHMARK(thinkMethodMinimax14Moves);
BENCHMARK(simulationMinimax);
BENCHMARK(thinkMethodOrangeMonkey);
BENCHMARK(thinkMethodOrangeMonkey14Moves);
BENCHMARK(simulationOrangeMonkey);

BENCHMARK_MAIN();
